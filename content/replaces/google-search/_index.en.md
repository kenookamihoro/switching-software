---
title: Google Search
subtitle: Search Engines
provider: google
order: 
    - duckduckgo
    - qwant
    - searx
aliases:
    - /ethical-alternatives-to-google-search/
---