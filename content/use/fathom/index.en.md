---
title: Fathom
icon: icon.svg
replaces:
    - google-analytics
---

**Fathom** is an analytics software that avoids tracking or storing personal data. The lite version is free and [open source][floss], and there is a paid "pro" version that is hosted by the makers of Fathom.

{{< infobox >}}
- **Website:**
    - [usefathom.com](https://usefathom.com)
- **Source Code:**
    - [github.com](https://github.com/usefathom/fathom)
    - [Lite vs. Pro](https://github.com/usefathom/fathom#lite-vs-pro)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}