---
slug: riot
title: Riot / Matrix
icon: icon.svg
replaces:
    - whatsapp
    - facebook-messenger
    - slack
---

**Riot.im** is a [libre][floss] instant messaging client based on the Matrix protocol. It includes text chats, audio/video calls and file transfers.

**The Matrix protocol** has a federated design and allows bridges to other communication apps. Furthermore, anyone can set up their own Matrix server and use it to collaborate with other people’s Matrix servers.

{{< infobox >}}
- **Sign up:**
    - [Matrix.org](https://riot.im/app/#/register)
    - [List of instances](https://www.hello-matrix.net/public_servers.php)
- **Desktop app:**
    - [Windows / macOS / Linux](https://riot.im/download/desktop/)
- **Mobile app:**
    - [Android](https://play.google.com/store/apps/details?id=im.vector.app) ([FDroid](https://f-droid.org/en/packages/im.vector.alpha/))
    - [iOS](https://itunes.apple.com/us/app/vector.im/id1083446067)
- **Web app**
    - [Riot.im](https://riot.im/app/)
- **Explore:**
    - [Matrix Project](https://matrix.org/)
    - [Matrix Clients](https://matrix.org/clients/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}