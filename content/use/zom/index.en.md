---
title: Zom
icon: icon.svg
replaces: 
    - whatsapp
    - facebook-messenger
---

**Zom** is an extremely easy to use and free open source messenger app. You don’t need to register to use it, just think of a username.

It’s compatible with the popular XMPP standard used by many other messenger apps, so if you know someone who uses one of those other apps you can message them with Zom.

{{< infobox >}}
- **Android app:** 
    - [Zom Mobile Messenger](https://play.google.com/store/apps/details?id=im.zom.messenger)
- **iOS app:**
    - [Zom Mobile Messenger](https://itunes.apple.com/app/zom-mobile-messenger/id1059530167)
{{< /infobox >}}